from otree.api import (
    models,
    widgets,
    BaseConstants,
    BaseSubsession,
    BaseGroup,
    BasePlayer,
    Currency as c,
    currency_range,
)
import logging
import random
from common.common import SharedConstants

logger = logging.getLogger(__name__)


doc = """
This is a public goods games, with 3 players who get unequal rewards for the final goods produced.
It runs over 5 rounds, with an belief ilicitation stage between rounds 1 and 2.

"""


class Constants(BaseConstants):
    name_in_url = 'unequal_equals'
    players_per_group = 3
    num_rounds = 6
    dropout_max = 3          # Number of consecutive dropout rounds before getting booted
    max_wait_time = 5 * 60  # The time the participants wait to be grouped

    # Various page timeouts (in seconds) - these are hidden from display so users don't feel pressured to make a decision,
    # but are needed for online experiments in case peope drop out mid round. Have set them to 5 min. which
    # should allow plenty of time
    contribute_timeout = 5 * 60
    belief_timeout = 5 * 60
    further_rounds_timeout = 5 * 60
    results_timeout = 5 * 60

    instructions_template_345COM = 'global/instructions345COM.html'
    instructions_template_345SUB = 'global/instructions345SUB.html'


class Subsession(BaseSubsession):
    def group_by_arrival_time_method(self, waiting_players):
        logger.info(f"In group by arrival time with {len(waiting_players)} players")

        if len(waiting_players) >= 3:
            waiting_players[0].participant.vars['dropout_count'] = 0
            waiting_players[1].participant.vars['dropout_count'] = 0
            waiting_players[2].participant.vars['dropout_count'] = 0

            return waiting_players[0], waiting_players[1], waiting_players[2]

        for p in waiting_players:
            if p.has_waited_too_long():
                p.participant.vars['unmatched'] = True
                p.participant.vars['dropout_count'] = 0
                return [p]      # A single player group

    def creating_session(self):
        if(self.round_number == 1 and 'payout_values' not in self.session.vars):
            self.session.vars['payout_values'] = SharedConstants.get_payout_values(self.session.config['treatment'])
            logger.info(f"Unequals App adding payout values: {self.session.vars['payout_values']}")

    def vars_for_admin_report(self):
        pass


class Group(BaseGroup):

    def set_payoffs(self):
        for p in self.get_players():
            try:
                my_id, _, _ = Player._get_group_ids(p.id_in_group)
                num_hards = sum(1 for other in p.get_others_in_group() if (
                    other.my_work_rate is not None and other.my_work_rate == "HARD"))
                p.payoff = self.session.vars['payout_values'][my_id-1][p.my_work_rate[0]][num_hards]

            except IndexError:
                # This means that one of the players dropped out in this round, so they don't get paid for it
                p.payoff = 0

    def set_final_payoffs(self):
        # Pick 1 random round for the three players in this group
        rand_round = random.randint(2, Constants.num_rounds)
        for p in self.get_players():
            p.calc_final_payoff(rand_round)


class Player(BasePlayer):
    comprehension_my_payoff1 = models.IntegerField(label="")
    comprehension_my_payoff2 = models.IntegerField(label="")
    comprehension_p1_payoff1 = models.IntegerField(label="")
    comprehension_p1_payoff2 = models.IntegerField(label="")
    comprehension_p2_payoff1 = models.IntegerField(label="")
    comprehension_p2_payoff2 = models.IntegerField(label="")

    my_work_rate = models.StringField()
    p2_work_rate = models.StringField()
    p3_work_rate = models.StringField()
    p2_belief_rate = models.StringField(choices=['NORMAL', 'HARD'], widget=widgets.RadioSelect)
    p3_belief_rate = models.StringField(choices=['NORMAL', 'HARD'], widget=widgets.RadioSelect)
    belief_bonus = models.CurrencyField()

    def get_sample_payoff_tables(self):
        if('my_rate' not in self.participant.vars):
            # We only need to do  this once. Originally had it in group_by_arrival_time_method, but group id is set AFTER
            # that call, so can't assign values based on group id there
            self.participant.vars['my_rate'] = c(SharedConstants.rates[self.id_in_group-1])

        my_id, p2_id, p3_id = Player._get_group_ids(self.id_in_group)

        pv = self.session.vars['payout_values']
        response = dict(p1=pv[my_id-1], p2=pv[p2_id-1], p3=pv[p3_id-1])
        return response

    # This is the live pages method that gets called when players click the Normal/Hard buttons
    # for the other players
    def live_other_player_guess(self, data):
        logger.info(f"In live method, data: {data}")
        if (data['player'] == 2):
            self.p2_work_rate = data['value']
        elif (data['player'] == 3):
            self.p3_work_rate = data['value']

        if (self.has_guessed()):
            my_id, p2_id, p3_id = Player._get_group_ids(self.id_in_group)
            pv = self.session.vars['payout_values']

            # Add the prompt at the top of the table first
            player_1_options = dict(player_1_prompt='The possible payoffs where player 2 works <strong>{}</strong> and player 3 works <strong>{}</strong>'.format(
                self.p2_work_rate, self.p3_work_rate))

            # Then load the appropriate values depending on selections
            if(self.p2_work_rate == "HARD" and self.p3_work_rate == "HARD"):
                logger.info("HH")
                player_1_options['H'] = [pv[my_id-1]['H'][2], pv[p2_id-1]['H'][2], pv[p3_id-1]['H'][2]]
                player_1_options['N'] = [pv[my_id-1]['N'][2], pv[p2_id-1]['H'][1], pv[p3_id-1]['H'][1]]
                logger.info(f"Options: {player_1_options}")
                return {self.id_in_group: player_1_options}

            if(self.p2_work_rate == "HARD" and self.p3_work_rate == "NORMAL"):
                logger.info("HN")
                player_1_options['H'] = [pv[my_id-1]['H'][1], pv[p2_id-1]['H'][1], pv[p3_id-1]['N'][2]]
                player_1_options['N'] = [pv[my_id-1]['N'][1], pv[p2_id-1]['H'][0], pv[p3_id-1]['N'][1]]
                logger.info(f"Options: {player_1_options}")
                return {self.id_in_group: player_1_options}

            if(self.p2_work_rate == "NORMAL" and self.p3_work_rate == "HARD"):
                logger.info("NH")
                player_1_options['H'] = [pv[my_id-1]['H'][1], pv[p2_id-1]['N'][2], pv[p3_id-1]['H'][1]]
                player_1_options['N'] = [pv[my_id-1]['N'][1], pv[p2_id-1]['N'][1], pv[p3_id-1]['H'][0]]
                logger.info(f"Options: {player_1_options}")
                return {self.id_in_group: player_1_options}

            else:
                logger.info("NN")
                player_1_options['H'] = [pv[my_id-1]['H'][0], pv[p2_id-1]['N'][1], pv[p3_id-1]['N'][1]]
                player_1_options['N'] = [pv[my_id-1]['N'][0], pv[p2_id-1]['N'][0], pv[p3_id-1]['N'][0]]
                logger.info(f"Options: {player_1_options}")
                return {self.id_in_group: player_1_options}

    def calc_belief_bonus(self):
        _, p2_id, p3_id = Player._get_group_ids(self.id_in_group)
        p2 = self.group.get_player_by_id(p2_id)
        p3 = self.group.get_player_by_id(p3_id)

        try:
            if (self.p2_belief_rate == "" or self.p3_belief_rate == ""):
                logger.info(f"Player {self.id_in_group} did not enter anything on belief page. Not paying bonus")
                self.belief_bonus = c(0)
            elif (p2.my_work_rate == self.p2_belief_rate and p3.my_work_rate == self.p3_belief_rate):
                self.belief_bonus = c(self.session.config['belief_bonus'])
                self.payoff += self.session.config['belief_bonus']
                logger.info(f"Player {self.id_in_group} got beliefs correct, adding bonus {self.payoff}")
            else:
                logger.info(f"Player {self.id_in_group} got beliefs incorrect, no bonus")
                self.belief_bonus = c(0)

        except IndexError:
            # One of the players has dropped out, or not contributed
            self.belief_bonus = c(0)

    def calc_final_payoff(self, rand_round):
        rounds = self.in_all_rounds()

        self.participant.vars['round_1_payoff'] = rounds[0].payoff
        # Rounds are not 0 based, they start at 1
        self.participant.vars['round_rand_payoff'] = rounds[rand_round-1].payoff
        self.participant.vars['belief_bonus'] = rounds[0].belief_bonus
        self.participant.vars['rand_round'] = rand_round

        self.participant.payoff = rounds[0].payoff + rounds[rand_round-1].payoff + rounds[0].belief_bonus

    def get_other_rates(self):
        _, p2_id, p3_id = Player._get_group_ids(self.id_in_group)
        return SharedConstants.rates[p2_id-1], SharedConstants.rates[p3_id-1]

    def get_rate(self):
        return self.participant.vars['my_rate']

    def has_guessed(self):
        return (self.p2_work_rate is not None and self.p3_work_rate is not None)

    def _get_group_ids(my_id):
        p2_id = 2
        p3_id = 3
        if(my_id == 2):
            p2_id = 1
        elif(my_id == 3):
            p2_id = 1
            p3_id = 2
        return my_id, p2_id, p3_id

    def has_waited_too_long(self):
        import time
        if('grouping_page_arrival_time' in self.participant.vars):
            return (time.time() - self.participant.vars['grouping_page_arrival_time']) > Constants.max_wait_time

    def gone_awol(self):
        if self.participant.vars['dropout_count'] == Constants.dropout_max or 'victim' in self.participant.vars:
            status = "Victim" if 'victim' in self.participant.vars else "Dropout"
            logger.info(f"Player {self} dropped out. Count {self.participant.vars['dropout_count']}, Status: {status}")
            return True
        else:
            return False
