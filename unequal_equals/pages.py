from common.common import SharedConstants
from ._builtin import Page, WaitPage
from otree.api import Currency as c, currency_range
from .models import Constants
import logging
from .models import Player

logger = logging.getLogger(__name__)


def updateDropoutStatus(input_status, player):

    if(input_status):
        # If a player didn't enter anything on the page, they are considered absent, or dropped out
        logger.info(f"Player id: {player.id} didn't input data. Assuming they have dropped out")
        player.participant.vars['dropout_count'] = player.participant.vars['dropout_count'] + 1
        player.participant.vars['dropout_round'] = player.round_number

        # If this is a 3rd Strike, process the other players, who may be victims
        if (player.participant.vars['dropout_count'] >= Constants.dropout_max):
            # Just in case this player has been flagged as victim when 2 players are absent
            if 'victim' in player.participant.vars:
                del player.participant.vars['victim']

            for other_player in player.get_others_in_group():
                # Sanity check the case where both the players have gone AWOL (Only victims get compensated)
                if other_player.participant.vars['dropout_count'] < Constants.dropout_max:
                    other_player.participant.vars['victim'] = True
                    other_player.participant.vars['dropout_round'] = player.round_number
    else:
        # In this case they have entered data and are back in the room
        player.participant.vars['dropout_count'] = 0


class GroupingWaitPage(WaitPage):
    group_by_arrival_time = True
    template_name = 'unequal_equals/GroupingWaitPage.html'

    def is_displayed(self):
        return self.round_number == 1

    def app_after_this_page(self, upcoming_apps):
        if 'unmatched' in self.player.participant.vars:
            logger.info(f"Player {self.player} not matched in {Constants.max_wait_time}s, moving them on")
            return upcoming_apps[-1]


class Comprehension(Page):
    form_model = 'player'
    form_fields = ['comprehension_my_payoff1', 'comprehension_my_payoff2', 'comprehension_p1_payoff1',
                   'comprehension_p1_payoff2', 'comprehension_p2_payoff1', 'comprehension_p2_payoff2', ]

    def is_displayed(self):
        return self.round_number == 1

    def vars_for_template(self):
        template_dict = self.player.get_sample_payoff_tables()
        template_dict['units_produced'] = SharedConstants.get_goods_produced(self.session.config['treatment'])
        return template_dict

    def error_message(self, values):
        my_id, p2_id, p3_id = Player._get_group_ids(self.player.id_in_group)
        pv = self.session.vars['payout_values']

        logger.info(f"id in group: {self.player.id_in_group}")
        logger.info(f"my_id, p2_id, p3_id: {my_id}, {p2_id}, {p3_id}")
        logger.info(f"pv: {pv}")

        p3HNN = pv[my_id-1]['H'][0]
        p4HNN = pv[p3_id-1]['N'][1]
        p5HNN = pv[p2_id-1]['N'][1]
        p3NHH = pv[my_id-1]['N'][2]
        p4NHH = pv[p3_id-1]['H'][1]
        p5NHH = pv[p2_id-1]['H'][1]

        logger.info(f"p3HNN,p4HNN,p5HNN,p3NHH,p4NHH,p5NHH: {p3HNN},{p4HNN},{p5HNN},{p3NHH},{p4NHH},{p5NHH}")

        solutions = dict(
            comprehension_my_payoff1=p3HNN,
            comprehension_p1_payoff1=p4HNN,
            comprehension_p2_payoff1=p5HNN,
            comprehension_my_payoff2=p3NHH,
            comprehension_p1_payoff2=p4NHH,
            comprehension_p2_payoff2=p5NHH,
        )

        responses = dict(
            comprehension_my_payoff1="Wrong value for your payout",
            comprehension_p1_payoff1="Wrong value for 5 Token player payout",
            comprehension_p2_payoff1="Wrong value for 3 Tokem player payout",
            comprehension_my_payoff2="Wrong value for your payout in",
            comprehension_p1_payoff2="Wrong value for 5 Token player payout",
            comprehension_p2_payoff2="Wrong value for 3 Token player payout",
        )

        error_messages = dict()
        for field_name in solutions:
            if values[field_name] != solutions[field_name]:
                error_messages[field_name] = responses[field_name]

        return error_messages


class PostComprehensionWaitPage(WaitPage):
    body_text = "Please wait for the other memebers of your group to complete the comprehension task"

    def is_displayed(self):
        return self.round_number == 1


class FurtherRounds(Page):
    timeout_seconds = Constants.further_rounds_timeout

    def is_displayed(self):
        return self.round_number == 2


class FurtherRoundsWaitPage(WaitPage):
    def is_displayed(self):
        return self.round_number == 2


class Contribute(Page):
    timeout_seconds = Constants.contribute_timeout
    live_method = 'live_other_player_guess'

    form_model = 'player'
    form_fields = ['my_work_rate']

    def vars_for_template(self):
        template_dict = self.player.get_sample_payoff_tables()
        template_dict['units_produced'] = SharedConstants.get_goods_produced(self.session.config['treatment'])
        return template_dict

    def before_next_page(self):
        updateDropoutStatus(self.player.my_work_rate == "", self.player)


class ResultsWaitPage(WaitPage):
    def after_all_players_arrive(self):
        self.group.set_payoffs()

    body_text = "Waiting for other participants to contribute."

    def app_after_this_page(self, upcoming_apps):

        if self.player.gone_awol():
            logger.info(f"Player {self.player} dropped out, moving group on")
            return upcoming_apps[-1]


class BeliefElicitation(Page):
    timeout_seconds = Constants.belief_timeout
    form_model = 'player'
    form_fields = ['p2_belief_rate', 'p3_belief_rate']

    def vars_for_template(self):
        p2_rate, p3_rate = self.player.get_other_rates()
        return dict(p2_rate=p2_rate, p3_rate=p3_rate)

    def is_displayed(self):
        return self.round_number == 1

    def before_next_page(self):
        self.player.calc_belief_bonus()
        updateDropoutStatus(self.player.p2_belief_rate == "" or self.player.p3_belief_rate == "", self.player)


class BeliefElicitationWaitPage(WaitPage):
    body_text = "Please wait for other participants to provide their estimates"

    def is_displayed(self):
        return self.round_number == 1

    def app_after_this_page(self, upcoming_apps):

        if self.player.gone_awol():
            logger.info(f"Player {self.player} dropped out, moving group on")
            return upcoming_apps[-1]


class Results(Page):
    timeout_seconds = Constants.results_timeout

    def vars_for_template(self):
        player1 = self.player
        other_players = player1.get_others_in_group()

        results = dict(
            P1=[player1.my_work_rate, player1.participant.vars['my_rate'], player1.payoff],
            P2=[other_players[0].my_work_rate, other_players[0].participant.vars['my_rate'], other_players[0].payoff],
            P3=[other_players[1].my_work_rate, other_players[1].participant.vars['my_rate'], other_players[1].payoff]
        )
        return results


class EndOfRoundWaitPage(WaitPage):
    body_text = "Please wait for the other players to finish reading their results"


class FinalWaitPage(WaitPage):
    after_all_players_arrive = 'set_final_payoffs'

    def is_displayed(self):
        return self.round_number == Constants.num_rounds


page_sequence = [GroupingWaitPage, Comprehension, PostComprehensionWaitPage, FurtherRounds, FurtherRoundsWaitPage, Contribute, ResultsWaitPage,
                 BeliefElicitation, BeliefElicitationWaitPage, Results, EndOfRoundWaitPage, FinalWaitPage]
