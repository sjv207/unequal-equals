from otree.api import (
    models,
    widgets,
    BaseConstants,
    BaseSubsession,
    BaseGroup,
    BasePlayer,
    Currency as c,
    currency_range,
)
import math
import logging


logger = logging.getLogger(__name__)


author = 'Scott Vincent'

doc = """
The last app in the sequence. Handles dropout notifications, payments and forwarding links
"""


class Constants(BaseConstants):
    name_in_url = 'completion'
    players_per_group = None
    num_rounds = 1
    dropout_max=3


class Subsession(BaseSubsession):
    pass


class Group(BaseGroup):
    pass


class Player(BasePlayer):
    status = models.StringField()

    def has_player_consented(self):
        if ('consent_dropout' in self.participant.vars):
            # Didn't consent, don't pay them anything
            self.status = 'Did not consent'
            self.participant.payoff = -self.session.config['participation_fee']
            return False
        else:
            return True

    def was_player_unmatched(self):
        if ('unmatched' in self.participant.vars):
            # Knock off the participation fee, add on the show up fee, convert to real money
            # (The participation fee gets added on by the system later)
            # self.participant.payoff = (self.session.config['show_up_fee'] - (self.session.config['participation_fee'])) * self.ecu_conversion_rate()
            self.participant.payoff = self.session.config['show_up_fee'] - self.session.config['participation_fee']

            self.status = 'Was unmatched'
            logger.info("unmatched True")
            return True
        else:
            logger.debug("unmatched False")
            return False

    def has_player_dropped_out(self):
        if ('dropout_count' in self.participant.vars and self.participant.vars['dropout_count'] == Constants.dropout_max):
            self.status = 'Dropped Out'
            return True
        else:
            return False

    def is_dropout_victim(self):
        if ('victim' in self.participant.vars):
            self.status = 'Drop Out Victim'
            self.participant.payoff = c(
                self.session.config['dropout_victim_payoff_per_round'] * 
                self.participant.vars['dropout_round'] /
                self.session.config['real_world_currency_per_point']
                )            
            return True
        else:
            return False

    def ecu_conversion_rate(self):
        conversion_rate = 1.0 / self.session.config['real_world_currency_per_point']
        return c(math.floor(conversion_rate))
