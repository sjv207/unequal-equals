from otree.api import Currency as c, currency_range
from ._builtin import Page, WaitPage
from .models import Constants
import logging


logger = logging.getLogger(__name__)


class ConsentDropout(Page):
    def is_displayed(self):
        return not self.player.has_player_consented()


class DroppedOut(Page):
    def is_displayed(self):
        return self.player.has_player_dropped_out()


class Unmatched(Page):
    def is_displayed(self):
        return self.player.was_player_unmatched()


class DropoutVictim(Page):
    def is_displayed(self):
        return self.player.is_dropout_victim()



class Results(Page):
    # This is here in case the Experimentor advances the slowest players, none of the above players should see this page
    def is_displayed(self):
        if (self.player.has_player_consented() and not self.player.has_player_dropped_out() and not self.player.is_dropout_victim()):
            return True
        else:
            return False

    def vars_for_template(self):
        # round_1_payoff, rand_round, round_rand_payoff, belief_bonus
        results = dict(
            round_1_payoff=self.participant.vars['round_1_payoff'],
            round_rand_payoff=self.participant.vars['round_rand_payoff'],
            belief_bonus=self.participant.vars['belief_bonus'],
            rand_round=self.participant.vars['rand_round'])
        logger.info(f"Results: {results}")
        return results


page_sequence = [ConsentDropout, Unmatched, DroppedOut, DropoutVictim, Results, ]
