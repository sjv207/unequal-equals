import logging


logger = logging.getLogger(__name__)



class SharedConstants:
    goods_produced_345COM = [20, 40, 65, 100]
    goods_produced_345SUB = [20, 55, 80, 100]
    rates = [3, 4, 5]
    hard_work_cost = 90

    def get_goods_produced(treatment):
        return SharedConstants.goods_produced_345SUB if treatment == "345SUB" else SharedConstants.goods_produced_345COM

    def get_payout_values(treatment):
        logger.info(f"Getting payouts for treatment: {treatment}")
        return SharedConstants.__build_payout_values(SharedConstants.goods_produced_345SUB, SharedConstants.rates, SharedConstants.hard_work_cost) if \
            treatment == "345SUB" else \
                SharedConstants.__build_payout_values(SharedConstants.goods_produced_345COM, SharedConstants.rates, SharedConstants.hard_work_cost)

    def __build_payout_values(goods_produced, rates, hard_work_cost):
        rate0 = dict(
            N=[
                goods_produced[0] * rates[0],
                goods_produced[1] * rates[0],
                goods_produced[2] * rates[0]
            ], H=[
                (goods_produced[1] * rates[0]) - hard_work_cost,
                (goods_produced[2] * rates[0]) - hard_work_cost,
                (goods_produced[3] * rates[0]) - hard_work_cost
            ])
        rate1 = dict(
            N=[
                goods_produced[0] * rates[1],
                goods_produced[1] * rates[1],
                goods_produced[2] * rates[1]
            ], H=[
                (goods_produced[1] * rates[1]) - hard_work_cost,
                (goods_produced[2] * rates[1]) - hard_work_cost,
                (goods_produced[3] * rates[1]) - hard_work_cost
            ])
        rate2 = dict(
            N=[
                goods_produced[0] * rates[2],
                goods_produced[1] * rates[2],
                goods_produced[2] * rates[2]
            ], H=[
                (goods_produced[1] * rates[2]) - hard_work_cost,
                (goods_produced[2] * rates[2]) - hard_work_cost,
                (goods_produced[3] * rates[2]) - hard_work_cost
            ])
        return [rate0, rate1, rate2]
