from otree.api import Currency as c, currency_range
from . import models
from ._builtin import Page, WaitPage
from .models import Constants
import math
import logging

logger = logging.getLogger(__name__)


class Introduction(Page):

    def before_next_page(self):
        # Make a record so we know how long they have been on the main app grouping page
        import time
        self.player.participant.vars['grouping_page_arrival_time'] = time.time()


class Consent(Page):
    timeout_seconds = Constants.consent_timeout
    form_model = models.Player
    form_fields = ['consent']
    timeout_submission = {'consent': False}

    def vars_for_template(self):
        return {'consent_timeout_min': math.ceil(self.timeout_seconds / 60)}

    def is_displayed(self):
        return self.round_number == 1

    def consent_error_message(self, value):
        if not value:
            return 'You must accept the consent form in order to proceed with the study!'

    def before_next_page(self):
        if self.timeout_happened:
            self.player.consent = False
            # Used in the next app to ensure the player doesn't play
            self.player.participant.vars['consent_dropout'] = True

    def app_after_this_page(self, upcoming_apps):
        logger.info(f"App after this page for {self.player}")
        if self.player.consent == False:
            logger.info(f"Player did not consent, moving them on")
            return upcoming_apps[-1]


class BlockDropouts(Page):
    def is_displayed(self):
        return self.round_number == 1 and self.player.consent == False


page_sequence = [
    Consent, Introduction, BlockDropouts,
]
