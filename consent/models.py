from os import environ
from otree.api import (
    models, widgets, BaseConstants, BaseSubsession, BaseGroup, BasePlayer,
    Currency as c, currency_range
)

from django import forms as djforms
from common.common import SharedConstants
import logging

logger = logging.getLogger(__name__)

author = 'Scott Vincent'

doc = """
Your app description
"""


class Constants(BaseConstants):
    name_in_url = 'consent'
    players_per_group = None
    num_rounds = 1
    consent_timeout = int(environ.get('CONSENT_TIMEOUT', 120))
    
    instructions_template_345COM = 'global/instructions345COM.html'
    instructions_template_345SUB = 'global/instructions345SUB.html'

    startwp_timer = 150


class Subsession(BaseSubsession):
    def creating_session(self):
        if(self.round_number == 1):
            self.session.vars['payout_values'] = SharedConstants.get_payout_values(self.session.config['treatment'])
            logger.info(f"Consent app adding payout values: {self.session.vars['payout_values']}, for treatment: {self.session.config['treatment']}")


class Group(BaseGroup):
    pass


class Player(BasePlayer):
    consent = models.BooleanField(widget=djforms.CheckboxInput, initial=False)


