from os import environ

# if you set a property in SESSION_CONFIG_DEFAULTS, it will be inherited by all configs
# in SESSION_CONFIGS, except those that explicitly override it.
# the session config can be accessed from methods in your apps as self.session.config,
# e.g. self.session.config['participation_fee']

SESSION_CONFIG_DEFAULTS = dict(
    real_world_currency_per_point=0.01427, participation_fee=3.00, doc=""
)

SESSION_CONFIGS = [
    dict(
        name='unequal_equals',
        display_name="Unequal Equals - TESTING ONLY: skip consent & comprehension",
        num_demo_participants=3,
        app_sequence=['unequal_equals','completion'],
        treatment = "345SUB",
        belief_bonus = 20,
        show_up_fee=5.00,
        dropout_victim_payoff_per_round=0.25,
    ),
    dict(
        name='consent',
        display_name="Unequal Equals Full",
        num_demo_participants=3,
        app_sequence=['consent','unequal_equals','completion'],
        belief_bonus = 20,
        treatment = "345SUB",
        dropout_victim_payoff_per_round=0.25,
        show_up_fee=5.00,
        doc="""
        The treatment can be 345COM, or 345SUB
        """
    ),
]


# ISO-639 code
# for example: de, fr, ja, ko, zh-hans
LANGUAGE_CODE = 'en'

# e.g. EUR, GBP, CNY, JPY
REAL_WORLD_CURRENCY_CODE = 'GBP'
USE_POINTS = True
POINTS_CUSTOM_NAME = 'Tokens'

ROOMS = []

ADMIN_USERNAME = 'admin'
# for security, best to set admin password in an environment variable
ADMIN_PASSWORD = environ.get('OTREE_ADMIN_PASSWORD')

DEMO_PAGE_INTRO_HTML = """ """

SECRET_KEY = '2bi1%#l1hiurwv75my$g55!pb3@g(bkw6znm64lo+s!8f$x0d&'

# if an app is included in SESSION_CONFIGS, you don't need to list it here
INSTALLED_APPS = ['otree']
